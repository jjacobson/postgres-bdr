#!/usr/bin/env python

from BaseHTTPServer import BaseHTTPRequestHandler
from helpers.etcd import Etcd
from helpers.postgresql import Postgresql
from helpers.ha import Ha
import os, sys, yaml, socket, logging

LOG_LEVEL = os.getenv('GOV_LOG_LEVEL', 'INFO')
if LOG_LEVEL == 'DEBUG':
    LOG_LEVEL = logging.DEBUG
elif LOG_LEVEL == 'WARNING':
    LOG_LEVEL = logging.WARNING
elif LOG_LEVEL == 'ERROR':
    LOG_LEVEL = logging.ERROR
elif LOG_LEVEL == 'CRITICAL':
    LOG_LEVEL = logging.CRITICAL
else:
    LOG_LEVEL = logging.INFO

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=LOG_LEVEL)

f = open(sys.argv[1], "r")
config = yaml.load(f.read())
f.close()

etcd = Etcd(config["etcd"])
postgresql = Postgresql(config["postgresql"])
ha = Ha(postgresql, etcd)

class StatusHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        return self.do_ANY()
    def do_OPTIONS(self):
        return self.do_ANY()
    def do_ANY(self):
        try:
            self.send_response(200 if ha.is_healthy() else 503)
            self.end_headers()
        except:
            pass
    def finish(self):
        try:
            BaseHTTPRequestHandler.finish(self)
        except:
            pass
    def handle(self):
        try:
            BaseHTTPRequestHandler.handle(self)
        except:
            pass

try:
    from BaseHTTPServer import HTTPServer
    host, port = config["status"]["listen"].split(":")
    server = HTTPServer((host, int(port)), StatusHandler)
    print 'listening on %s:%s' % (host, port)
    server.serve_forever()
except KeyboardInterrupt:
    print('^C received, shutting down server')
    server.socket.close()
