import urllib2, json, os, time, base64, ssl
import logging
from urllib import urlencode
import helpers.errors

class Etcd:
    def __init__(self, config):
        self.scope = config["scope"]
        self.endpoint = config["endpoint"]
        if config.has_key("authentication"):
            self.authentication = config["authentication"]
        else:
            self.authentication = None
        self.ttl = config["ttl"]
        self.timeout = config["timeout"]
        self.ready = False

    def can_connect(self):
        try:
            request = urllib2.Request(self.client_url())
            urllib2.urlopen(request, timeout=self.timeout).read()
            return True
        except (urllib2.HTTPError, urllib2.URLError, ssl.SSLError) as err:
            logging.warning('Cannot connect to etcd')
            logging.warning(err)
        return False

    def get_client_path(self, path, max_attempts=1):
        attempts = 0
        response = None

        while True:
            try:
                request = urllib2.Request(self.client_url(path))
                if self.authentication is not None:
                    base64string = base64.encodestring('%s:%s' % (self.authentication["username"], self.authentication["password"])).replace('\n', '')
                    request.add_header("Authorization", "Basic %s" % base64string)
                response = urllib2.urlopen(request, timeout=self.timeout).read()
                break
            except (urllib2.HTTPError, urllib2.URLError, ssl.SSLError) as e:
                attempts += 1
                if attempts < max_attempts:
                    logging.warning("Failed to return %s, trying again. (%s of %s)" % (path, attempts, max_attempts))
                    time.sleep(3)
                else:
                    raise e
        try:
            return json.loads(response)
        except ValueError:
            return response

    def put_client_path(self, path, data):
        request = urllib2.Request(self.client_url(path), data=urlencode(data).replace("false", "False"))
        if self.authentication is not None:
            base64string = base64.encodestring('%s:%s' % (self.authentication["username"], self.authentication["password"])).replace('\n', '')
            request.add_header("Authorization", "Basic %s" % base64string)
        request.get_method = lambda: 'PUT'
        urllib2.urlopen(request, timeout=self.timeout).read()

    def client_url(self, path=''):
        return "%s/v2/keys/service/%s%s" % (self.endpoint, self.scope, path)

    def current_leader(self):
        try:
            hostname = self.get_client_path("/leader")["node"]["value"]
            address = self.get_client_path("/members/%s" % hostname)["node"]["value"]

            return {"hostname": hostname, "address": address}
        except urllib2.HTTPError as e:
            if e.code == 404:
                return None
            raise helpers.errors.CurrentLeaderError("Etcd is not responding properly")
        except (urllib2.URLError, ssl.SSLError):
            raise helpers.errors.CurrentLeaderError("Etcd is not responding properly")

    def members(self):
        try:
            members = []

            r = self.get_client_path("/members?recursive=true")
            if 'node' not in r or 'nodes' not in r['node']:
                return None

            for node in r["node"]["nodes"]:
                members.append({"hostname": node["key"].split('/')[-1], "address": node["value"]})

            return members
        except urllib2.HTTPError as e:
            if e.code == 404:
                return None
            raise helpers.errors.CurrentLeaderError("Etcd is not responding properly")
        except (urllib2.URLError, ssl.SSLError):
            raise helpers.errors.CurrentLeaderError("Etcd is not responding properly")

    def touch_member(self, member, connection_string, ready=False):
        self.put_client_path("/members/%s" % member, {"value": connection_string, "ttl": self.ttl})
        if ready:
            self.put_client_path("/ready/%s" % member, {"value": "true", "ttl": self.ttl})

    def take_leader(self, value):
        return self.put_client_path("/leader", {"value": value, "ttl": self.ttl}) == None

    def attempt_to_acquire_leader(self, value):
        try:
            return self.put_client_path("/leader", {"value": value, "ttl": self.ttl, "prevExist": False}) == None
        except urllib2.HTTPError as e:
            if e.code == 412:
                logging.warn("Could not take out TTL lock: %s" % e)
            return False
        except (urllib2.URLError, ssl.SSLError):
            return False

    def update_leader(self, state_handler):
        try:
            self.put_client_path("/leader", {"value": state_handler.name, "ttl": self.ttl, "prevValue": state_handler.name})
        except (urllib2.HTTPError, urllib2.URLError) as e:
            logging.error("Error updating leader lock and optime on ETCD for primary.")
            return False

    def leader_unlocked(self):
        try:
            self.get_client_path("/leader")
            return False
        except urllib2.HTTPError as e:
            if e.code == 404:
                return True
            return False
        except urllib2.URLError:
            return False
        except ValueError:
            return False
        except ssl.SSLError:
            return False

    def am_i_leader(self, value):
        try:
            reponse = self.get_client_path("/leader")
            logging.debug("Leader: %s; I am %s" % (reponse["node"]["value"], value))
            return reponse["node"]["value"] == value
        except (urllib2.HTTPError, ssl.SSLError, urllib2.URLError):
            logging.error("Couldn't reach etcd")
            return False

    def race(self, path, value):
        while True:
            try:
                return self.put_client_path(path, {"prevExist": False, "value": value}) == None
            except urllib2.HTTPError as e:
                if e.code == 412:
                    return False
                else:
                    logging.warning("etcd is not ready for connections")
                    time.sleep(10)
            except (urllib2.URLError, ssl.SSLError):
                    logging.warning("Issue connecting to etcd")
                    time.sleep(10)
