from __future__ import print_function
import os, psycopg2, re, time
import logging

from urlparse import urlparse

class Postgresql:

    def __init__(self, config):
        self.name = config["name"]
        self.host, self.port = config["listen"].split(":")
        self.data_dir = config["data_dir"]
        self.replication = config["replication"]
        self.databases = config['databases'].split(',') if 'databases' in config and config['databases'] is not None else []
        self.extensions = config['extensions'].split(',') if 'extensions' in config and config['extensions'] is not None else []
        if 'btree_gist' not in self.extensions:
            self.extensions.append('btree_gist')

        if 'bdr' not in self.extensions:
            self.extensions.append('bdr')

        if 'uuid-ossp' not in self.extensions:
            self.extensions.append('uuid-ossp')

        self.config = config

        self.cursor_holder = None
        self.connection_string = "postgres://%s:%s@%s:%s/postgres" % (self.replication["username"], self.replication["password"], self.host, self.port)

        self.conn = None

    def cursor(self, dbname='postgres'):
        if not self.cursor_holder:
            self.conn = psycopg2.connect(self.local_connection_string(dbname=dbname))
            self.conn.autocommit = True
            self.cursor_holder = self.conn.cursor()

        return self.cursor_holder

    def local_connection_string(self, dbname='postgres'):
        connection_string = ["dbname=%s" % dbname, "port=%s" % self.port]

        if self.config.get("use_tcp_for_local_connection") or self.config["parameters"].get("unix_socket_directories") == "":
            connection_string.append("host=%s" % self.host)
        elif self.config["parameters"].get("unix_socket_directories"):
            explicit_socket_directory = self.config["parameters"]["unix_socket_directories"].split(" ")[0]
            connection_string.append("host=%s" % explicit_socket_directory)

        return " ".join(connection_string)

    def disconnect(self):
        try:
            self.conn.close()
        except Exception as e:
            logging.error("Error disconnecting: %s" % e)

    def query(self, sql, params=None):
        max_attempts = 0
        while True:
            try:
                self.cursor().execute(sql, params)
                break
            except psycopg2.OperationalError as e:
                if self.conn:
                    self.disconnect()
                self.cursor_holder = None
                if max_attempts > 4:
                    raise e
                max_attempts += 1
                time.sleep(5)
        return self.cursor()

    def data_directory_empty(self):
        return not os.path.exists(self.data_dir) or os.listdir(self.data_dir) == []

    def initialize(self, leader=None):
        if os.system("initdb %s" % self.initdb_options()) == 0:
            logging.info('Database initialized.')
        else:
            logging.info('Database already exists')

        self.write_pg_hba()

        # start Postgres without options to setup replication user indepedent of other system settings
        os.system("pg_ctl start -w -D %s -o '%s'" % (self.data_dir, self.server_options()))

        self.create_replication_user()
        self.create_databases()
        self.initialize_bdr(leader=leader)

        os.system("pg_ctl stop -w -m fast -D %s" % self.data_dir)

    def initialize_bdr(self, leader=None):
        logging.info('Governor initializing BDR')
        leader = urlparse(leader['address']) if leader is not None else None
        for db in self.databases:
            cur = self.cursor(dbname=db)
            values = {
                'db': db,
                'name': self.name,
                'host': self.host,
                'port': self.port,
                'user': self.replication['username']
            }
            if leader is None:
                try:
                    cur.execute("""SELECT bdr.bdr_group_create(
                            local_node_name := '%(name)s',
                            node_external_dsn := 'port=%(port)s dbname=%(db)s host=%(host)s user=%(user)s'
                        )""" % values)
                    logging.info('Governor: created BDR group.')
                except psycopg2.OperationalError as err:
                    logging.info('Governor: BDR group already exists.')
                cur.execute('SELECT bdr.bdr_node_join_wait_for_ready()')
                logging.info('Governor: BDR node ready.')
            else:
                values['leader_hostname'] = leader.hostname
                values['leader_port'] = leader.port
                try:
                    cur.execute("""SELECT bdr.bdr_group_join(
                            local_node_name := '%(name)s',
                            node_external_dsn := 'port=%(port)s dbname=%(db)s host=%(host)s user=%(user)s',
                            join_using_dsn := 'port=%(leader_port)s dbname=%(db)s host=%(leader_hostname)s user=%(user)s'
                        )""" % values)
                    logging.info('Governor: joined BDR group.')
                except psycopg2.OperationalError as err:
                    logging.info('Governor: Already part of the BDR group.')
                cur.execute('SELECT bdr.bdr_node_join_wait_for_ready()')
                logging.info('Governor: BDR node ready.')

            cur.close()
            self.cursor_holder = None
        logging.info('Governor: BDR initialized.')

    def is_running(self):
        return os.system("pg_ctl status -D %s > /dev/null" % self.data_dir) == 0

    def start(self):
        if self.is_running():
            logging.error("Cannot start PostgreSQL because one is already running.")
            return False

        pid_path = "%s/postmaster.pid" % self.data_dir
        if os.path.exists(pid_path):
            os.remove(pid_path)
            logging.info("Removed %s" % pid_path)

        return os.system("pg_ctl start -w -D %s -o '%s'" % (self.data_dir, self.server_options())) == 0

    def stop(self):
        return os.system("pg_ctl stop -w -D %s -m fast -w" % self.data_dir) != 0

    def reload(self):
        return os.system("pg_ctl reload -w -D %s" % self.data_dir) == 0

    def restart(self):
        return os.system("pg_ctl restart -w -D %s -m fast" % self.data_dir) == 0

    def server_options(self):
        options = "-c listen_addresses=%s -c port=%s" % (self.host, self.port)
        for setting, value in self.config["parameters"].iteritems():
            if value is None:
                continue

            options += " -c \"%s=%s\"" % (setting, value)
        return options

    def initdb_options(self):
        options = "-D %s" % self.data_dir
        if "initdb_parameters" in self.config:
            for param in self.config["initdb_parameters"]:
                options += " \"%s\"" % param
        return options

    def create_databases(self):
        for db in self.databases:
            cur = self.cursor()
            values = (db,)
            # check if db exists
            cur.execute('SELECT 1 from pg_database WHERE datname = %s', values)
            if cur.fetchone() is None:
                # create database
                while True:
                    try:
                        cur.execute('CREATE DATABASE %s' % values)
                        break
                    except psycopg2.OperationalError:
                        time.sleep(5)
                        continue
                logging.info('Governor created database: %s' % values)

            # connect to that DB and set up extensions
            cur.close()
            self.cursor_holder = None
            cur = self.cursor(dbname=db)
            for extension in self.extensions:
                try:
                    cur.execute('CREATE EXTENSION "%s"' % extension)
                except psycopg2.OperationalError:
                    # these could already exist
                    pass

            cur.close()
            self.cursor_holder = None

    def write_pg_hba(self):
        f = open("%s/pg_hba.conf" % self.data_dir, "w")
        # "local" is for Unix domain socket connections only
        print("local all all trust", file=f)
        # IPv4 local connections:
        print("host all all 127.0.0.1/32 trust", file=f)
        # IPv6 local connections:
        print("host all all ::1/128 trust", file=f)
        print("host all all all trust", file=f)
        print("host replication %s all trust" % self.replication["username"], file=f)
        f.close()

    def user_exists(self, user):
        return self.query('SELECT 1 FROM pg_user WHERE usename = %s', (user,)).fetchone() != None

    def create_replication_user(self):
        user = self.replication["username"]
        if not self.user_exists(user):
            self.query("CREATE USER \"%s\" WITH REPLICATION SUPERUSER;" % user)
            logging.info('Created replication user')
