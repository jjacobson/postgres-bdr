import sys, time, re, urllib2, json, psycopg2
import logging
from base64 import b64decode

import helpers.errors

import inspect

def lineno():
    """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno

class Ha:
    def __init__(self, postgresql, etcd):
        self.postgresql = postgresql
        self.etcd = etcd

    def is_healthy(self):
        if not self.postgresql.is_running():
            logging.warning("Not healthy. Postgresql is not running.")
            return False

        if not self.etcd.can_connect():
            logging.warning("Not healthy. Cannot connect to etcd")
            return False

        return True

    def acquire_lock(self):
        return self.etcd.attempt_to_acquire_leader(self.postgresql.name)

    def update_lock(self):
        return self.etcd.update_leader(self.postgresql)

    def is_unlocked(self):
        return self.etcd.leader_unlocked()

    def is_leader(self):
        return self.etcd.am_i_leader(self.postgresql.name)

    def fetch_current_leader(self):
        return self.etcd.current_leader()

    def run_cycle(self):
        try:
            # check if postgres was stopped and restart
            if not self.postgresql.is_running():
                self.postgresql.start()
                return "postgresql was stopped. starting again."

            if self.is_leader():
                # if currently the leader, continue being leader
                # so others can join the cluster
                self.update_lock()
            elif self.is_unlocked():
                # if there is no leader, race to become leader
                if self.acquire_lock():
                    return "became the new leader."
                else:
                    return "attempted to become leader but failed."

            return "no action. postgresql running"
        except helpers.errors.CurrentLeaderError:
            logging.error("failed to fetch current leader from etcd")
        except psycopg2.OperationalError:
            logging.error("Error communicating with Postgresql.  Will try again.")
        except helpers.errors.HealthiestMemberError:
            logging.error("failed to determine healthiest member fromt etcd")

    def run(self):
        while True:
            self.run_cycle()
            time.sleep(10)
