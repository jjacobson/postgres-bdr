#!/usr/bin/env python

import sys, os, yaml, time, urllib2, atexit, ssl
import logging

from helpers.etcd import Etcd
from helpers.postgresql import Postgresql
from helpers.ha import Ha


LOG_LEVEL = os.getenv('GOV_LOG_LEVEL', 'INFO')
if LOG_LEVEL == 'DEBUG':
    LOG_LEVEL = logging.DEBUG
elif LOG_LEVEL == 'WARNING':
    LOG_LEVEL = logging.WARNING
elif LOG_LEVEL == 'ERROR':
    LOG_LEVEL = logging.ERROR
elif LOG_LEVEL == 'CRITICAL':
    LOG_LEVEL = logging.CRITICAL
else:
    LOG_LEVEL = logging.INFO

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=LOG_LEVEL)

# stop postgresql on script exit
def stop_postgresql(postgresql):
    postgresql.stop()

# wait for etcd to be available
def wait_for_etcd(message, etcd, postgresql):
    etcd.ready = False
    while not etcd.ready:
        try:
            etcd.touch_member(postgresql.name, postgresql.connection_string)
            etcd.ready = True
        except (urllib2.URLError, ssl.SSLError) as e:
            logging.warn(e)
            logging.warn("waiting on etcd: %s" % message)
            time.sleep(5)

def run(config):
    etcd = Etcd(config["etcd"])
    postgresql = Postgresql(config["postgresql"])
    ha = Ha(postgresql, etcd)

    atexit.register(stop_postgresql, postgresql)
    logging.info("Governor Starting up")

    # is data directory empty?
    if postgresql.data_directory_empty():
        logging.info("Governor Starting up: Empty Data Dir")
    else:
        logging.info("Governor Starting up: Existing Data Dir")

    # racing to first
    wait_for_etcd("cannot initialize member without ETCD", etcd, postgresql)
    if etcd.race("/leader", postgresql.name):
        logging.info("Governor Starting up: Initialization Race ... WON!!!")
        logging.info("Governor Starting up: Initialize Postgres")
        postgresql.initialize()
        logging.info("Governor Starting up: Initialize Complete")
        etcd.take_leader(postgresql.name)
        logging.info("Governor Starting up: Starting Postgres")
        postgresql.start()
    else:
        logging.info("Governor Starting up: Initialization Race ... LOST")
        initialized = False
        while not initialized:
            leader = etcd.current_leader()
            if not leader:
                logging.info('Governor Starting up: Could not find leader. Trying again in 5 seconds')
                time.sleep(5)
                continue
            else:
                logging.info('Governor Starting up: Found leader %s' % leader['hostname'])
                logging.info("Governor Starting up: Initialize Postgres")
                postgresql.initialize(leader=leader)
                initialized = True

        logging.info("Governor Starting up: Starting Postgres")
        postgresql.start()

    wait_for_etcd("cannot participate in cluster HA without etcd", etcd, postgresql)
    logging.info("Governor Running: Starting Running Loop")
    while True:
        try:
            logging.info("Governor Running: %s" % ha.run_cycle())

            etcd.touch_member(postgresql.name, postgresql.connection_string, ready=True)

            time.sleep(config["loop_wait"])
        except urllib2.URLError as err:
            logging.warning("Lost connection to etcd, setting no leader and waiting on etcd")
            logging.warning(err)
            wait_for_etcd("running in readonly mode; cannot participate in cluster HA without etcd", etcd, postgresql)

if __name__ == "__main__":
    f = open(sys.argv[1], "r")
    config = yaml.load(f.read())
    f.close()

    run(config)
