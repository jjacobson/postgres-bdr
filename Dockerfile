FROM postgres:9.4

RUN apt-get update \
    && apt-get install -y wget \
    && echo 'deb http://packages.2ndquadrant.com/bdr/apt/ jessie-2ndquadrant main' > /etc/apt/sources.list.d/2ndquadrant.list \
    && wget --quiet -O - http://packages.2ndquadrant.com/bdr/apt/AA7A6805.asc | apt-key add - \
    && apt-get purge -y --auto-remove wget postgresql-9.4 \
    && apt-get update \
    && apt-get install -y \
        python \
        python-yaml \
        python-psycopg2 \
        postgresql-bdr-9.4 \
        postgresql-bdr-9.4-bdr-plugin \
        curl

COPY governor /governor

RUN mkdir -p /governor-config /home/postgres \
    && chown -R postgres:postgres /governor-config /governor /home/postgres

COPY governor-setup.sh docker-entrypoint.sh /usr/local/bin/

USER postgres
WORKDIR /governor

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ./governor.py /governor-config/config.yml
HEALTHCHECK --interval=15s --timeout=3s CMD curl --fail -XOPTIONS http://localhost:15432 || exit 1