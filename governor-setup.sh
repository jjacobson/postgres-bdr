#!/bin/bash
set -e

echo "Creating config file..."

cat > /governor-config/config.yml << EOF
loop_wait: 10
etcd:
  scope: governor
  ttl: 30
  endpoint: $ETCD_ENDPOINT
  timeout: 5
status:
  listen: 0.0.0.0:15432
postgresql:
  name: $HOSTNAME
  listen: ${HOSTNAME}${DOMAIN}:5432
  data_dir: ${PGDATA}/${HOSTNAME}
  databases: ${DATABASES:-postgres}
  maximum_lag_on_failover: 1048576 # 1 megabyte in bytes
  replication:
    username: ${REPLICATION_USER:-replicator}
    password: ${REPLICATION_PASS:-rep-pass}
    network:  ${REPLICATION_NETWORK:-0.0.0.0/0}
  parameters:
    shared_preload_libraries: 'bdr'
    track_commit_timestamp: 'true'
    default_sequenceam: 'bdr'
    archive_mode: "off"
    wal_level: logical
    max_wal_senders: 5
    wal_keep_segments: 8
    archive_timeout: 1800s
    max_replication_slots: 5
    hot_standby: "on"
EOF